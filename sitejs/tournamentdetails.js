$(function() {
    getdetails();
});

var sno_price = 0;
var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

function getdetails() {
    $.ajax({
        url: retrievedetails_api + sessionStorage.t_id + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".allimages").empty();
            for (let i = 0; i < data.images.length; i++) {
                $(".allimages").append(`
                   <div class="col-md-4">
                    <img src='${data.images[i].image}' class="h100" style="width:100%">
                   </div>
                `);
            }
            $("#priceamount_tbody").empty();
            for (var i = 0; i < data.prizes.length; i++) {
                sno_price++;
                $("#priceamount_tbody").append(` <tr>
                                        <td class="text-center">${sno_price}</td>
                                        <td>${data.prizes[i].tag}</td>
                                        <td><span>${data.prizes[i].amount ? "Rs." + data.prizes[i].amount : 'Not Available'}</span></td>
                                        <td>${data.prizes[i].description ? data.prizes[i].description : 'Not Available'}</td>
                                    </tr>`)
            }
            $(".tr_name").text(data.name);
            var startdate = monthNames[parseInt(data.start_date.substring(5, 7)) - 1] + " " + data.start_date.substring(0, 4) + ", " + data.start_date.substring(8, 10);
            var enddate = monthNames[parseInt(data.end_date.substring(5, 7)) - 1] + " " + data.end_date.substring(0, 4) + ", " + data.end_date.substring(8, 10);
            var closingdate = monthNames[parseInt(data.entry_closing_date.split("-")[1]) - 1] + " " + parseInt(data.entry_closing_date.split("-")[0]) + ", " + parseInt(data.entry_closing_date.split("-")[2]);
            $(".tr_closing").text(closingdate);
            $(".tr_date").text("From " + startdate + " To " + enddate);
            $(".tr_time").text(data.time);
            $(".tr_maxentries").text(data.max_entries ? data.max_entries : 'Not Available' );
            $(".tr_rules").append(data.rules);
            $(".tr_addr").text(data.address.street + " , " + data.address.area.name + " , " + data.address.city + " , " + data.address.state + " , " + data.address.country + " , " + data.address.zipcode + " .");

            $(".dyn_organiserli").empty();
            for (var i = 0; i < data.organizers.length; i++) {
                $(".dyn_organiserli").append(`<div class="col-md-4 brline">
                                    <div class="contactaddress">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p><b>Name :</b> ${data.organizers[i].name}</p>
                                                <p><b>Email :</b> <span>${data.organizers[i].email ? data.organizers[i].email : 'Not Available'}</span></p>
                                                <p><b>Phone-no :</b> ${data.organizers[i].contact_number} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>`);
            }
            $(".tr_stadium").text(data.stadium.name);
       

            $(".dyn_rulesli").empty();
            for (var i = 0; i < data.categories.length; i++) {
                $(".dyn_rulesli").append(` <div class="col-md-12">
                                                <ul>
                                                    <li>${data.categories[i].category} - ${data.categories[i].price}</li>
	
                                                </ul>
                                            </div>`);
            }

        },
        error: function(data) {
            console.log('error occured in tournament');
        }
    });
}
