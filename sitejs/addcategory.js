$(function() {
    listdata();

    document.getElementById('add_image').onchange = function() {
        $(".add_picname").text(this.value);
    };
    document.getElementById('edit_image').onchange = function() {
        $(".edit_picname").text(this.value);
    };

});

function listdata() {
    $(".dyn_list").empty();
    $.ajax({
        url: listcategories_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            if (data.length == 0) {
                $(".dyn_list").append(` <div class="col-md-12"><center><img src="images/nodata.png" style="width:30%"><p class="nolistdata" style="font-size: 20px;color: #f0555e;">No Data Found</p></center></div>`);
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".dyn_list").append(`<div class="col-sm-3">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-10">
                                    <p class="addserviceheading c_name${data[i].id}">${data[i].name}</p>
                                </div>
                                <div class="col-sm-2">
                                    <a href="#editcategorymodal" onclick="editthis(${data[i].id})" data-toggle="modal" class="pointer savebtn">Edit</a>
                                </div>
                            </div>
                            <hr class="mt-0 mb-10">
                            <a class="sports-bannerimage1" style="background-image:url(${(data[i].image ? data[i].image : "images/nodata.png")})"></a>
                        </div>
                    </div>`)
                }
            }
        },
        error: function(data) {
            $(".dyn_list").append(` <div class="col-md-12"><center><img src="images/nodata.png" style="width:30%"><p class="nolistdata" style="font-size: 20px;color: #f0555e;">No Data Found</p></center></div>`);
        }
    });
}

function addcategory() {
    var postData = new FormData();

    if ($('#add_image')[0].files.length == 0) {
        $("#snackbarerror").text("Catgeory Picture is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("image", $('#add_image')[0].files[0]);
    }

    if ($('#add_name').val() == '') {
        $('#add_name').addClass('iserr');
        $("#snackbarerror").text("Sports Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("name", $('#add_name').val());
    }

    $(".add_Btnadd_Btn").html(`Submit &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    $.ajax({
        url: createcategories_api,
        type: 'post',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".add_Btn").html(`Submit`).attr("disabled", false);
            $("#addcategorymodal").modal("toggle");
            $("#snackbarsuccs").text("Category Has Been Created Successfully!");
            showsuccesstoast();
            $(".add_picname").text("");
            $("#add_image,#add_name").val("");
            listdata();
        },
        error: function(data) {
            $(".add_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

var categoryid = '';

function editthis(id) {
    $("#edit_name").val($(".c_name" + id).text());
    categoryid = id;
}

function editcategory() {
    var postData = new FormData();

    if ($('#edit_image')[0].files.length != 0) {
        postData.append("image", $('#edit_image')[0].files[0]);
    }

    if ($('#edit_name').val() == '') {
        $('#edit_name').addClass('iserr');
        $("#snackbarerror").text("Sports Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("name", $('#edit_name').val());
    }

    $(".edit_Btn").html(`Submit &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    $.ajax({
        url: editcategories_api+categoryid+'/',
        type: 'PUT',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".edit_Btn").html(`Submit`).attr("disabled", false);
            $("#editcategorymodal").modal("toggle");
            $("#snackbarsuccs").text("Category Has Been Updated Successfully!");
            showsuccesstoast();
            $(".edit_picname").text("");
            $("#edit_image,#edit_name").val("");
            listdata();
        },
        error: function(data) {
            $(".edit_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}