$(() => {
    listtour();
    $(".addldr").hide();
});

var page_no = 1;

const gallerylist = Url => {
    $.ajax({
        url: Url,
        type: "get",
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
        },
        success: data => {
            $(".allimages,.pagination").empty();
            if (data.results.length == 0) {
                $(".pagination").empty();
                $(".allimages").append(`
						<center>
							<img src="iamges/nodata.png" class="img-responsive" style="height: 200px">
						</center>
            		`);
            } else {
                data.results.map(x => {
                    $(".allimages").append(`
						<div class="col-md-4">
							<img src="${x.image}" class="img-responsive" style="height: 200px">
						</div>
            		`);
                });
                pagination(data.next_url, data.prev_url, data.count, data.page_size, $(".tt_name").val());
            }
        },
        error: data => {
            $(".allimages,.pagination").empty();
            $(".allimages").append(`
					<center>
						<img src="iamges/nodata.png" class="img-responsive" style="height: 200px">
					</center>
        		`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}


function addphotos(e) {
    if (window.File && window.FileList && window.FileReader) {
        $(".tour_img").empty();
        addprod_images = [];
        var files = e.target.files;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            addprod_images.push(file);
            add_img_Files(file, i);
        }
        console.log(addprod_images);
    } else {
        alert("Your browser doesn't support to File API")
    }
}

function add_img_Files(f, i, type) {
    var reader = new FileReader();
    reader.onload = (function(file) {
        return function(e) {
            $(".tour_img").append(`
            <div class="col-md-3">
                <img src="${e.target.result}" class="img-responsive" style="width:100%: height:50px;">
            </div>
        `);
        };
    })(f, i);
    reader.readAsDataURL(f);
}

const creategallery = () => {
    if (addprod_images.length === 0) {
        // $('.tour_name').addClass("iserr");
        $("#snackbarerror").text("Image is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($(".tournament_id").val() == '') {
        // $('.tour_name').addClass("iserr");
        $("#snackbarerror").text("Tournament is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    var postData = new FormData();

    for (var i = 0; i < addprod_images.length; i++) {
        postData.append('images', addprod_images[i]);
    }

    postData.append('tournament', $(".tournament_id").val());
    $(".addbtn").attr("disabled", true);
    $(".addldr").show();
    $.ajax({
        url: gallerycreate_api,
        type: "post",
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
        },
        success: function(data) {
            $(".tt_name").val($(".tournament_id").val());
            $("#snackbarsuccs").text("Images added");
            showsuccesstoast();
            page_no = 1;
            gallerylist(gallery_api + $(".tournament_id").val() + '/');
            $(".addcls").click();
            $(".addbtn").attr("disabled", false);
            $(".addldr").hide();
        },
        error: function(data) {
        	$(".addcls").click();
            $(".addbtn").attr("disabled", false);
            $(".addldr").hide();
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

const listtour = () => {
    $.ajax({
        url: tourlist_api,
        type: "get",
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function(data) {
            $(".tour_name").empty();
            $(".tour_name").append(`<option value=''>Select tournament</option>`);
            data.map(x => {
                $(".tour_name").append(`<option value='${x.id}'>${x.name}</option>`);
            });
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

//pagination starts here
const pagination = (next, prev, count, size, val) => {
    $(".pagination").empty();
    var pages = Math.ceil(count / size);
    if (pages > 1) {
        prev ? $(".pagination").append(`<li class="pageprev page-item"><a class="pointer cptr page-link" onclick="prevpage('${prev}')">Previous</a></li>`) : '';
        for (var i = (page_no < 3 ? 1 : page_no - 2); i <= pages && i <= (page_no < 3 ? page_no + (5 - page_no) : page_no + 2); i++) {
            $(".pagination").append(`<li class="pgs pg${i} page-item"><a class="pointer cptr page-link" onclick="navigate(${i},${val})">${i}</a></li>`);
        }
        next ? $(".pagination").append(`<li class="pagenext page-item"><a class="pointer cptr page-link" onclick="nextpage('${next}')">Next</a></li>`) : '';
    }
    $(".pgs").removeClass("active");
    $(".pg" + page_no).addClass("active");
}

const navigate = (page, val) => {
    page_no = Number(page);
    gallerylist(gallery_api + val + '/' + "?page=" + page_no);
}

const nextpage = next => {
    page_no++;
    gallerylist(next);
}

const prevpage = prev => {
    page_no--;
    gallerylist(prev);
}
//pagination ends here

const gallerys = el => {
    if ($(el).val()) {
        page_no = 1;
        gallerylist(gallery_api + $(el).val() + '/');
    }
}