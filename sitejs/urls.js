// var domain = "http://192.168.1.11:8000/";
var domain = "http://pro-tournament.zordec.com/";

//auth api
var register_api = domain + 'auth/register/';
var verifyotp_api = domain + 'auth/activate/account/';
var changephno_api = domain + 'auth/update/mobile-number/';
var login_api = domain + 'auth/login/';
var forgotpwd_api = domain + 'auth/forgot-password/';
var resetpassword_api = domain + 'auth/reset-password/';
var changepassword_api = domain + 'auth/change-password/';
var logout_api = domain + 'auth/logout/';

//add catgory page api
var listcategories_api = domain + 'sports/';
var createcategories_api = domain + 'sports/';
var editcategories_api = domain + 'sports/';

//users api
var listusers_api = domain + 'super-admin/list/users/';
var resetpwdinusers_api = domain + 'super-admin/reset-password/';

//listorgaiser page api
var listorgnisers_api = domain + 'super-admin/list/organizers/';
var acceptorg_api = domain + 'super-admin/accept/organizers/';

// list tournaments page api
var listtournaments_api = domain + 'super-admin/list/tournaments/';
var accepttournament_api = domain + 'super-admin/accept/tournaments/';

var retrievedetails_api = domain + 'tournament/retrieve/details/';

//bookings pag api
var listing_api = domain + 'booking/list/';

var gallery_api = domain + 'photo-gallery/list/';
var gallerycreate_api = domain + 'photo-gallery/create/';

var tourlist_api = domain + 'photo-gallery/list/completed-tournaments/';
